<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'test2' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Oc;h(:kbFq4SNcLbs(~I4,,(umK]e};L0G)#FS:]b&r&u$B4a1Hk@s1mD}Wy:0w&' );
define( 'SECURE_AUTH_KEY',  'J+/9yTgXP[:=hDPY9+0Kr|j#GVOo6NdBV_a-t{wx 8!W,mk}Rh|Uen]pH.zIt}4j' );
define( 'LOGGED_IN_KEY',    'X^p[<5&t{1yhcwm^:=24L|aXE&&:]3mCD$tkZ!xO^Oa!UWg5a(NjAc9(*rgpR:Uq' );
define( 'NONCE_KEY',        'F95vE/2]It#d[qX1V8DkrZRWW=pYjX~V0M/a0XC98:EHJg7r=->YM#2,3DI)VUhp' );
define( 'AUTH_SALT',        'l~ev/[Ul;5f(MI>+{(&DjdTH7Fli([(E&BDLn`2#cd)ws9A*DU*aoO<<6m-cz-) ' );
define( 'SECURE_AUTH_SALT', 'e]d+xDu^zQA3 Lu1q/E__zi_+7L)qsX!mCxQl.=*s49v:12k;XmDSUcp<)@CmMdy' );
define( 'LOGGED_IN_SALT',   'z,;@M)^E=~?r!Igl3>5?1%8,p^E+_W3g?3MYmCnM|PzoGjK:g]_zubxo*lc*bV~*' );
define( 'NONCE_SALT',       'fgV(+ys^%/Facq1Cq:+UU6l_y3GXh37#A]jr$g_G2Oei*@7nx_cq[3.A> L+J$_:' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
